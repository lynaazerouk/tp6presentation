package courielrevision;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class TestCourielrevision {
   private Couriel c ;
	
	@BeforeEach 
	void setup () 
	{ 
		c= new Couriel("m@lyna.lyna", "titre", "bnj madame monsieur pj");
		
	}
	
	@Test
	void testadresse()
	{
		 assertTrue (c.adressemail());
	}
	@Test
	void testtitre()
	{
		assertTrue(c.ptitre());	
		assertFalse ( new Couriel (null,null,null).ptitre());
	}
	@ParameterizedTest
	@ValueSource(strings = {"bnj madame monsieur pj"  , "bnj madame monsieur pj" })
	void testcorps(String  corps)
	{
		assertTrue(new Couriel (null,null,corps).contient());
		
	}
	@Test
	void corps_malfait(){
		assertFalse(new Couriel (null,null,null).contient());
		assertFalse(new Couriel (null,null,"bnj madame monsieur").contient());
	}
	@ParameterizedTest
	@ValueSource(strings = {"a@hbhjbhhh"  , "j@hfhjg" })
	 void testparametreradr(String adr )
	{
		 Couriel  cc =new Couriel (adr,null,null);
		 assertFalse(cc.adressemail());
		 assertFalse (new Couriel (null,null,null).adressemail());
		   
	}

	
	static Stream<Arguments> adresseprovider()
	{
		return Stream.of(Arguments.of("djfdj", "pas de @ ni de .") , Arguments.of("h@fhdbf","manque le point "));
	}
	
	@ParameterizedTest(name ="{index}=> adr={0}, cause={1}")
	@MethodSource("adresseprovider")
	 void adresseparametrersource(String adr ,String cause )
	{     Couriel cc=  new Couriel(adr,null,null)  ;
	      assertFalse(cc.adressemail(),cause);       //  dans un test faut jamais oublier de faire appele a la methode qu'on veut tester 
		                                       // assertThrow on le fait uniquement pour tester si une excepetion est bien declancher alors que la on a aucune exception 
	}
	
	
	
	
	
	
	
	
	}
	

